# Travail pratique 3

## Description

Jeu de platforme utilisant la librairie SDL dans le cadre du cours INF3135 à l'UQAM.

## Auteurs

- Adem BROURI (BROA23089702) (@doudoumi)
- Rémi FLEURANCE (FLER28089609) (@remifleurance)
- Guillaume ALABRE (ALAG28049606) (@homersimpsons)

## Fonctionnement

Au lancement du programme vous arriverez sur le menu d'accueil.

Pressez <Echap> pour quitter le jeu, ou <Espace> pour lancer une partie.

## Plateformes supportées

Le jeu a été compilé et testé sous Ubuntu 18.04.

## Dépendances

* [SDL2](https://www.libsdl.org/)
* [SDL2_image](https://www.libsdl.org/projects/SDL_image/)

## Compilation

Assurez-vous de bien avoir installé les dépendances (`libsdl2-dev` et `libsdl2-image-dev`).

Pour compiler :
```
make
```

Pour executer :
```
./tp3
```

## Références

La structure de base du code est basée sur [`Maze-SDL` de A. Blondin](https://bitbucket.org/ablondin-projects/maze-sdl)

## Division des tâches

La division des tâches n'a pas été gérée via ce `README`. Nous avons séparé les tâches en différentes issues. Cela permet d'attribuer l'issue, et de la résoudre une fois la tâche terminée.

## Statut

Le jeu de base est terminé. On peut se déplacer, interragir avec le décor et des dessins ont été ajoutés pour chaques éléments.

On peut cependant imaginer une amélioration du jeu avec des dessins plus variés et des animations qui n'ont pas été ajoutées pour le moment.
