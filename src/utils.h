#ifndef UTILS_H
#define UTILS_H

// -------------- //
// Data structure //
// -------------- //

enum Action {   // Elementary actions
    MOVE_RIGHT, // Right
    MOVE_LEFT,  // Left
    JUMP,		// Jump
};

struct Point { // Representing a 2D point
    int x;     // The x-coordinate
    int y;     // The y-coordinate
};

#endif
