#include "constants.h"
#include "character.h"
#include "level.h"
#include <math.h>

struct Character *Character_create(SDL_Renderer *renderer)
{
    struct Character *character;
    character = (struct Character *)malloc(sizeof(struct Character));
    character->renderer = renderer;
    character->screenPosition.x = CHAR_INITIAL_X;
    character->screenPosition.y = CHAR_INITIAL_Y;
    character->speed.x = 0;
    character->speed.y = 0;
    return character;
}

void Character_delete(struct Character *character)
{
    if (character != NULL)
    {
        free(character);
    }
}

void Character_render(struct Character *character)
{
    SDL_Rect fillRect;
    fillRect.x = character->screenPosition.x;
    fillRect.y = character->screenPosition.y;
    fillRect.w = CHAR_WIDTH;
    fillRect.h = CHAR_HEIGHT;
    SDL_SetRenderDrawColor(character->renderer, 0xFF, 0x00, 0x00, 0xFF);
    SDL_RenderFillRect(character->renderer, &fillRect);
    fillRect.x = character->screenPosition.x + CHAR_WIDTH/2 - (CHAR_DIR_SIZE/2) + (character->speed.x * CHAR_WIDTH / (2 * MAX_SPEED_X));
    fillRect.y = character->screenPosition.y + CHAR_HEIGHT/2 - (CHAR_DIR_SIZE/2) + (character->speed.y * CHAR_HEIGHT / (2 * MAX_SPEED_Y));
    fillRect.w = CHAR_DIR_SIZE;
    fillRect.h = CHAR_DIR_SIZE;
    SDL_SetRenderDrawColor(character->renderer, 0x5F, 0x97, 0xFF, 0xFF);
    SDL_RenderFillRect(character->renderer, &fillRect);
}

void Character_action(struct Character *character,
                      enum Action action, struct Level *level)
{
    struct Point position;
    switch (action)
    {
    case MOVE_RIGHT:
        character->speed.x += MAX_SPEED_X;
        if (character->speed.x > MAX_SPEED_X)
        {
            character->speed.x = MAX_SPEED_X;
        }
        break;
    case MOVE_LEFT:
        character->speed.x -= MAX_SPEED_X;
        if (character->speed.x < -MAX_SPEED_X)
        {
            character->speed.x = -MAX_SPEED_X;
        }
        break;
    case JUMP:
        position.x = character->screenPosition.x;
        position.y = character->screenPosition.y + 1;
        if (!Level_isValidPosition(position) || Level_isInCollision(level, position))
        {
            character->speed.y -= MAX_SPEED_Y;
        }
        break;
    }
}
void doSubMovement(struct Character *character, struct Level *level)
{
    struct Point position;
    double stepX, stepY;
    int max = 0;
    if (abs(character->speed.x) >= abs(character->speed.y) && character->speed.x != 0)
    {
        stepX = character->speed.x > 0 ? 1 : -1;
        stepY = character->speed.y / (double)abs(character->speed.x);
        max = abs(character->speed.x);
    }
    else if (character->speed.y != 0)
    {
        stepX = character->speed.x / (double)abs(character->speed.y);
        stepY = character->speed.y > 0 ? 1 : -1;
        max = abs(character->speed.y);
    }

    double posX = character->screenPosition.x, posY = character->screenPosition.y;
    int i = 0;
    for (i = 0; i < max; i++)
    {
        posY += stepY;
        position.x = (int)posX;
        position.y = (int)posY;
        if (!Level_isValidPosition(position) || Level_isInCollision(level, position))
        {
            posY -= stepY;
            character->speed.y = 0;
        }
        posX += stepX;
        position.x = (int)posX;
        position.y = (int)posY;
        if (!Level_isValidPosition(position) || Level_isInCollision(level, position))
        {
            posX -= stepX;
        }
    }
    character->screenPosition.x = (int)posX;
    character->screenPosition.y = (int)posY;
}
void applyForces(struct Character *character, struct Level *level)
{
    struct Point position;
    position.x = character->screenPosition.x;
    position.y = character->screenPosition.y + 1;
    if (Level_isValidPosition(position) && !Level_isInCollision(level, position))
    {
        character->speed.y += 3;
        if (character->speed.y > MAX_SPEED_Y) {
            character->speed.y = MAX_SPEED_Y;
        }
    }
    else
    {
        character->speed.y = 0;
    }

    character->speed.x /= 2;
}
void Character_move(struct Character *character, struct Level *level)
{
    doSubMovement(character, level);
    applyForces(character, level);
}
