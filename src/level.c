#include "level.h"
#include "stdbool.h"
#include "utils.h"
#include "constants.h"
#include "tile.h"


bool Level_isValidPosition(struct Point position) {
    // Check in screen
    if (position.x < 0
        || position.x + CHAR_WIDTH > SCREEN_WIDTH
        || position.y < 0
        || position.y + CHAR_HEIGHT > SCREEN_HEIGHT) {
            return false;
        }
    return true;
}

/**
 * Check a collision between a tile and the character
 *
 * @param	tile 		Tile to check
 * @pâram	position	Player position
 * @return	true is the is a collision, false otherwise
 */
bool checkCollision(struct Tile *tile, struct Point position) {
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    leftA = tile->screenPosition.x;
    rightA = tile->screenPosition.x + tile->dimensions.x;
    topA = tile->screenPosition.y;
    bottomA = tile->screenPosition.y + tile->dimensions.y;

    leftB = position.x;
    rightB = position.x + CHAR_WIDTH;
    topB = position.y;
    bottomB = position.y + CHAR_HEIGHT;

    if(bottomB <= topA) return false;
    if(topB >= bottomA) return false;
    if(rightB <= leftA) return false;
    if(leftB >= rightA) return false;

    return true;
}

bool Level_isInCollision(struct Level *level, struct Point position) {
	int i;
	for(i = 0; i < level->numberTiles; i++) {
		if (checkCollision(level->tiles[i], position)) {
            if (level->tiles[i]->type == BEIGNE) {
                level->tiles[i]->renderer = NULL;
                level->tiles[i]->screenPosition.x = 0;
                level->tiles[i]->screenPosition.y = 0;
                level->tiles[i]->dimensions.x = 0;
                level->tiles[i]->dimensions.y = 0;
                level->score += 1;
            } else {
                return true;
            }
        }
	}
	return false;
}

struct Level *Level_create(SDL_Renderer *renderer) {
    struct Level *level;
    level = (struct Level*)malloc(sizeof(struct Level));
    level->numberTiles = 7;
    level->tiles = malloc(sizeof(struct Tile*) * level->numberTiles);
    level->tiles[0] = Tile_create(renderer, 250, SCREEN_HEIGHT - 30, 100, 30, WALL);
    level->tiles[1] = Tile_create(renderer, 400, SCREEN_HEIGHT - 150, 100, 30, WALL);
    level->tiles[2] = Tile_create(renderer, 0, SCREEN_HEIGHT - 300, 300, 30, WALL);
    level->tiles[3] = Tile_create(renderer, 600, SCREEN_HEIGHT - 100, 300, 100, WALL);
    level->tiles[4] = Tile_create(renderer, 550, SCREEN_HEIGHT - 300, 100, 30, WALL);
    level->tiles[5] = Tile_create(renderer, 600, SCREEN_HEIGHT - 350, 30, 30, BEIGNE);
    level->tiles[6] = Tile_create(renderer, 30, SCREEN_HEIGHT - 350, 30, 30, BEIGNE);
    level->score = 0;
    level->scoreMax = 2;
    return level;
}

void Level_delete(struct Level *level) {
    if (level != NULL) {
        free(level);
    }
    int i = 0;
    for(i = 0; i < level->numberTiles; i++) {
    	if (level->tiles[i] != NULL) free(level->tiles[i]);
    }
}

void Level_render(struct Level *level) {
    int i = 0;
    for(i = 0; i < level->numberTiles; i++) {
    	Tile_render(level->tiles[i]);
    }
}
