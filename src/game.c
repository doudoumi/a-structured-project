#include "game.h"
#include "sdl2.h"
#include "constants.h"
#include "level.h"

#define KEY_RIGHT (1 << 0)
#define KEY_LEFT  (1 << 1)
#define KEY_UP    (1 << 2)

struct Game *Game_initialize(SDL_Renderer *renderer) {
    struct Game *game;
    game = (struct Game*)malloc(sizeof(struct Game));
    game->renderer = renderer;
    game->level = Level_create(renderer);
    game->character = Character_create(renderer);
    game->state = GAME_PLAY;
    return game;
}

void Game_delete(struct Game *game) {
    if (game != NULL) {
        Level_delete(game->level);
        Character_delete(game->character);
        free(game);
    }
}

/**
 * Gives the time to wait if needed until the next frame.
 *
 * @param   nextFrame   Expected tick for the next frame
 * @return  time to wait in milliseconds
 */
Uint32 waitNextFrame(Uint32 nextFrame) {
    Uint32 currentFrame = SDL_GetTicks();
    if(nextFrame <= currentFrame) {
        return 0;
    } else {
        return nextFrame - currentFrame;
    }
}

void Game_run(struct Game *game) {
    SDL_Event e;
    char keysPressed = 0;
    Uint32 nextFrame;
    nextFrame = SDL_GetTicks() + 1000 / FRAME_RATE;
    while (1) {
        while (SDL_PollEvent(&e) != 0) {
            switch (e.type) {
                case SDL_QUIT:
                    game->state = GAME_QUIT;
                    return;
                case SDL_KEYDOWN:
                    switch (e.key.keysym.sym) {
                        case SDLK_RIGHT:
                            keysPressed |= KEY_RIGHT;
                            break;
                        case SDLK_LEFT:
                            keysPressed |= KEY_LEFT;
                            break;
                        case SDLK_UP:
                            keysPressed |= KEY_UP;
                            break;
                    }
                    break;
                case SDL_KEYUP:
                    switch (e.key.keysym.sym) {
                        case SDLK_RIGHT:
                            keysPressed &= ~KEY_RIGHT;
                            break;
                        case SDLK_LEFT:
                            keysPressed &= ~KEY_LEFT;
                            break;
                        case SDLK_UP:
                            keysPressed &= ~KEY_UP;
                            break;
                    }
                    break;
            }
        }
        if (keysPressed & KEY_RIGHT) {
            Character_action(game->character, MOVE_RIGHT, game->level);
        }
        if (keysPressed & KEY_LEFT) {
            Character_action(game->character, MOVE_LEFT, game->level);
        }
        if (keysPressed & KEY_UP) {
            Character_action(game->character, JUMP, game->level);
        }
        Character_move(game->character, game->level);
        SDL_SetRenderDrawColor(game->renderer, 0x5F, 0x97, 0xFF, 0x00 );
        SDL_RenderClear(game->renderer);
        Level_render(game->level);
        Character_render(game->character);

        SDL_Delay(waitNextFrame(nextFrame));
        nextFrame += 1000 / FRAME_RATE;

        SDL_RenderPresent(game->renderer);

        if (game->level->score == game->level->scoreMax) {
            return;
        }
    }
}
