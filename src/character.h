#ifndef CHARACTER_H
#define CHARACTER_H

#include "utils.h"
#include "sdl2.h"
#include "level.h"

// --------------- //
// Data structures //
// --------------- //

struct Character
{
    struct Point screenPosition; // His position in the screen
    struct Point speed;          // His current speed
    SDL_Renderer *renderer;      // The renderer
};

// --------- //
// Functions //
// --------- //

/**
 * Creates the character.
 *
 * @param renderer   The renderer
 * @return           A pointer to the character, NULL if there was an error;
 */
struct Character *Character_create(SDL_Renderer *renderer);

/**
 * Deletes the character.
 *
 * @param character  The character to delete
 */
void Character_delete(struct Character *character);

/**
 * Renders the character.
 *
 * @param character  The character to render
 */
void Character_render(struct Character *character);

/**
 * Perform an action by the character
 *
 * @param character  The characte that should perform the action
 * @param action     The action
 * @param level      Current level
 */
void Character_action(struct Character *character,
                      enum Action action, struct Level *level);

/**
 * Moves the character.
 *
 * @param character  The character to move
 * @param level 	 Curent level
 */
void Character_move(struct Character *character, struct Level *level);
/**
 * Slow down x speed and add gravity
 *
 * @param character  The character to move
 * @param level 	 Curent level
 */
void applyForces(struct Character *character, struct Level *level);
/**
 *Change the player position according to his speed (verifying collision pixel by pixel)
 *
 * @param character  The character to move
 * @param level 	 Curent level
 */

void doSubMovement(struct Character *character, struct Level *level);
#endif
