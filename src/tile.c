#include "constants.h"
#include "tile.h"
#include "spritesheet.h"

struct Tile *Tile_create(SDL_Renderer *renderer, int xPos, int yPos, int width, int height, enum TileType type) {
    struct Tile *tile;
    tile = (struct Tile*)malloc(sizeof(struct Tile));
    tile->renderer = renderer;
    tile->screenPosition.x = xPos;
    tile->screenPosition.y = yPos;
    tile->dimensions.x = width;
    tile->dimensions.y = height;
    tile->type = type;
    return tile;
}

void Tile_delete(struct Tile *tile) {
    if (tile != NULL) {
        free(tile);
    }
}

void Tile_render(struct Tile *tile) {
    SDL_Rect rect;
    if (tile->type == WALL) {
        SDL_Surface *wall = IMG_Load(WALL_FILENAME);
        SDL_Texture *wall_tx = SDL_CreateTextureFromSurface(tile->renderer, wall);
        SDL_FreeSurface(wall);
        SDL_SetTextureBlendMode(wall_tx, SDL_BLENDMODE_NONE);
        int w, h;
        SDL_QueryTexture(wall_tx, NULL, NULL, &w, &h);
        for (int x = 0; x < tile->dimensions.x; x += w) {
            for (int y = 0; y < tile->dimensions.y; y += h) {
                rect.x = tile->screenPosition.x + x;
                rect.y = tile->screenPosition.y + y;
                rect.w = x + w <= tile->dimensions.x ? w : tile->dimensions.x - x;
                rect.h = y + h <= tile->dimensions.y ? h : tile->dimensions.y - y;
                SDL_RenderCopy(tile->renderer, wall_tx, NULL, &rect);
            }
        }
        SDL_DestroyTexture(wall_tx);
    } else if (tile->type == BEIGNE && tile->renderer != NULL) {
        SDL_Surface *beigne = IMG_Load(BEIGNE_FILENAME);
        SDL_Texture *beigne_tx = SDL_CreateTextureFromSurface(tile->renderer, beigne);
        SDL_FreeSurface(beigne);
        rect.x = tile->screenPosition.x;
        rect.y = tile->screenPosition.y;
        rect.w = BEIGNE_SIZE;
        rect.h = BEIGNE_SIZE;
        SDL_RenderCopy(tile->renderer, beigne_tx, NULL, &rect);
        SDL_DestroyTexture(beigne_tx);
    }
}
