#ifndef TILE_H
#define TILE_H

#include "utils.h"
#include "sdl2.h"

// --------------- //
// Data structures //
// --------------- //

enum TileType {
    WALL,
    BEIGNE
};

struct Tile {
    struct Point screenPosition;   // Tile position in screen
    struct Point dimensions;       // Tile dimensiosn (width & height)
    SDL_Renderer *renderer;        // The renderer
    enum TileType type;
};

// --------- //
// Functions //
// --------- //

/**
 * Creates the tile.
 *
 * @param renderer  The renderer
 * @param xPos      The x position on screen
 * @param yPos      The y position on screen
 * @param width		Tile x size
 * @param height	TIle y size
 * @return          A pointer to the tile, NULL if there was an error;
 */
struct Tile *Tile_create(SDL_Renderer *renderer, int xPos, int yPos, int width, int height, enum TileType type);

/**
 * Deletes the tile.
 *
 * @param Tile  The tile to delete
 */
void Tile_delete(struct Tile *Tile);

/**
 * Renders the tile.
 *
 * @param Tile  The tile to render
 */
void Tile_render(struct Tile *Tile);

#endif
