#ifndef CONSTANTS_H
#define CONSTANTS_H

// General constants
#define SCREEN_WIDTH  900
#define SCREEN_HEIGHT (900 * 4 / 5)
#define FRAME_RATE 60

// Menu constants
#define TITLE_FILENAME "assets/menu/title.png"
#define TITLE_WIDTH 800
#define TITLE_X ((SCREEN_WIDTH - TITLE_WIDTH) / 2)
#define TITLE_Y 32
#define START_FILENAME "assets/menu/start.png"
#define START_WIDTH 465
#define PLAY_X ((SCREEN_WIDTH - START_WIDTH) / 2)
#define PLAY_Y 490
#define QUIT_FILENAME "assets/menu/quit.png"
#define QUIT_WIDTH 465
#define QUIT_X ((SCREEN_WIDTH - QUIT_WIDTH) / 2)
#define QUIT_Y 552

// Character constants
#define MAX_SPEED_X 10
#define MAX_SPEED_Y 30
#define CHAR_WIDTH 20
#define CHAR_HEIGHT 20
#define CHAR_INITIAL_X 0
#define CHAR_INITIAL_Y (SCREEN_HEIGHT - CHAR_HEIGHT)
#define CHAR_DIR_SIZE 4

// Game Constants
#define WALL_FILENAME "assets/wall.png"
#define BEIGNE_FILENAME "assets/donut.png"
#define BEIGNE_SIZE 30

#endif
