#ifndef LEVEL_H
#define LEVEL_H

#include "utils.h"
#include "stdbool.h"
#include "tile.h"

// --------------- //
// Data structures //
// --------------- //

struct Level {
	int numberTiles;			//The number of tiles
	struct Tile **tiles;		//Tiles representing the level
	int score;					// The player score
	int scoreMax;				// Max score of the level
};

// --------- //
// Functions //
// --------- //

/**
 * Check if the current level position is valid
 *
 * @param position The position
 * @return Is the position valid ?
 */
bool Level_isValidPosition(struct Point position);

/**
 * Check if the character is in collision with any tile from the level
 * If it's a beigne tile then the score is automatically increased
 *
 * @param level     The level
 * @param position  Player position
 * @return		    true if the character is in collision, false otherwise
 */
bool Level_isInCollision(struct Level *level, struct Point position);

/**
 * Creates the level.
 *
 * @param renderer   The renderer
 * @return           A pointer to the level, NULL if there was an error;
 */
struct Level *Level_create(SDL_Renderer *renderer);

/**
 * Deletes the level.
 *
 * @param level  The level to delete
 */
void Level_delete(struct Level *level);

/**
 * Renders the level.
 *
 * @param level  The level to render
 */
void Level_render(struct Level *level);

#endif
