CC = gcc
SOURCE_DIR = src
BUILD_DIR = build
OBJS = $(patsubst $(SOURCE_DIR)/%.c,$(BUILD_DIR)/%.o,$(wildcard $(SOURCE_DIR)/*.c))
CFLAGS = --std=c11 -W -Wall `sdl2-config --cflags`
LDFLAGS = -lSDL2_image `sdl2-config --libs`
EXEC = tp3

$(EXEC): $(OBJS)
	mkdir -p $(BUILD_DIR)
	$(CC) $^ $(LDFLAGS) -o $(EXEC)

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.c $(BUILD_DIR)
	$(CC) $< $(CFLAGS) -c -o $@

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

.PHONY: clean

clean:
	rm -rf $(BUILD_DIR)
